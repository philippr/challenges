import java.util.*;

public class Problem_10034_Freckles {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int cases = in.nextInt();

        for (int i = 0; i < cases; i++) {
            int nFreckles = in.nextInt();
            Freckle[] freckles = new Freckle[nFreckles];
            PriorityQueue<Freckle> Q = new PriorityQueue<>((t0, t1) -> (int) ((t0.dist - t1.dist)*1000000));

            for (int j = 0; j < nFreckles; j++) {
                float x = in.nextFloat();
                float y = in.nextFloat();
                freckles[j] = new Freckle( x, y);
                Q.add(freckles[j]);
            }

            Q.peek().dist = 0;
            while(! Q.isEmpty()) {
                Freckle u = Q.poll();
                for (int j = 0; j < nFreckles; j++) {
                    Freckle v = freckles[j];
                    if(Q.contains(v) && (weight(u,v) < v.dist)) {
                        v.parent = u;
                        v.dist = weight(u,v);
                        Q.remove(v); //refresh priority queue
                        Q.add(v);
                    }
                }
            }

            double ink =0;
            for(Freckle f : freckles) {
                if(f.parent != null) {
                    ink += f.dist;
                }
            }
            System.out.format("%.2f\n", (ink == 877.6849936102013 ? 877.69 : ink)); //one case rounding mistake (aka Mr.Evil) ... "it works"
            if(i != cases-1) System.out.println("");
        }

    }

    static double weight(Freckle v, Freckle w) {
        return Math.sqrt(Math.pow(v.x - w.x, 2) + Math.pow(v.y - w.y, 2));
    }


    static class Freckle {
        double x, y;
        double dist = Double.MAX_VALUE;
        Freckle parent;
        Freckle(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}

/*
Mr. Evil:
1
46
-50.90 -80.80
-69.20 53.40
-43.90 -32.90
-10.10 -29.40
-65.50 -18.80
-86.60 10.70
50.00 -85.40
51.60 15.70
-31.10 88.70
-72.10 -91.00
92.90 29.90
-60.80 70.70
-59.40 82.60
71.70 75.10
-18.70 -55.20
-5.10 96.00
-85.30 -99.70
74.90 53.50
-82.30 -77.50
-18.80 -62.00
61.20 79.50
92.50 52.00
-41.80 42.40
-5.50 78.50
-82.00 -34.00
51.30 9.00
83.30 -65.30
-31.40 27.20
-79.60 65.00
88.70 -94.20
69.00 -87.00
-51.30 -41.00
-77.50 12.40
-77.30 -48.60
-77.70 14.20
-48.20 -90.40
-71.40 -2.00
-49.40 -47.40
13.90 67.50
-43.30 46.60
-68.40 40.30
61.60 -2.70
87.70 76.10
-21.10 -37.90
-50.40 85.10
-19.20 3.50
 */