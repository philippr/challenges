import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

import static java.lang.Math.*;

public class Problem_12345_ABF_Lock {

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer tokenizer = new StringTokenizer(in.readLine());
        int cases = Integer.parseInt(tokenizer.nextToken());

        for (int i = 0; i < cases; i++) {
            tokenizer = new StringTokenizer(in.readLine());
            int n = Integer.parseInt(tokenizer.nextToken());
            int[] combinations = new int[n];
            int[] distances = new int[10000];
            PriorityQueue<Integer> Q = new PriorityQueue<>((t0, t1) -> (distances[t0] - distances[t1]));
            ArrayList<Integer> MST = new ArrayList<>(n);


            for (int j = 0; j < n; j++) {
                combinations[j] = Integer.parseInt(tokenizer.nextToken());
                Q.add(combinations[j]);
                distances[combinations[j]] = Integer.MAX_VALUE;
            }

            int minStart = Integer.MAX_VALUE;

            while(! Q.isEmpty()) {
                int curr = Q.poll();

                int startDist = dist(0,curr);
                minStart = (startDist < minStart ? startDist : minStart);

                for (int j = 0; j < n; j++) {
                    int next = combinations[j];
                    if(Q.contains(next) && dist(next, curr) < distances[next]) {
                        distances[next] = dist(next, curr);
                        if(! MST.contains(next)) MST.add(next);
                        Q.remove(next); //refresh priority queue
                        Q.add(next);
                    }
                }
            }

            int combis = 0;
            for(int m : MST) {
                combis += distances[m];
            }

            System.out.println(combis+minStart);

        }
    }

    public static int dist(int t0, int t1) {

        int mini = min(t0%10, t1%10);
        int maxi = max(t0%10, t1%10);

        if(t0 < 10 && t1 < 10) {
            return min(maxi-mini, (mini+10) -maxi);
        }

        return dist(t0/10, t1/10) + min(maxi-mini,(mini+10) - maxi);
    }

}