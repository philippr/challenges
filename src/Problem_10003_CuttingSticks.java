import java.util.Scanner;

public class Problem_10003_CuttingSticks { //change classname to "Main" for the judge
    static int[] c;
    static int[][] cost;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int l,n;

        while((l = in.nextInt()) != 0) {
            n = in.nextInt();
            c = new int[n+2];
            for (int i = 0; i < n; i++) {
                c[i+1] = in.nextInt();
            }

            c[0] = 0;
            c[n+1] = l;
            cost = new int[n+2][n+2];
            int length = n + 2;

            for (int j = 1; j < length; j++) {      //iterate diagonal through matrix
                for (int i = 0; i < length-j; i++) {
                    cost[i][i+j] = cut(i,i+j);
                }
            }
            System.out.println("The minimum cutting is " + cost[0][n+1] + ".");
        }
    }

    public static int cut(int i, int j) {
        if((i+1) == j)
            return 0;

        int min = Integer.MAX_VALUE;
        for (int k = i+1; k < j; k++) {
            min = Math.min((cost[i][k] + cost[k][j]), min);
        }
        return min  + (c[j] - c[i]);
    }

}
