import java.util.Scanner;

public class Problem_10739_StringtoPalindrome {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        for (int i = 0; i < n; i++) {
            int changes = 0;
            String curr = in.next();
            int length = curr.length();
            System.out.println(palin(curr));
        }
    }

    public static int palin(String s) {
     //   System.out.println(s);
        if(s.length() <= 1)
            return 0;
        if (s.substring(0, 1).equals(s.substring(s.length() - 1, s.length()))) {
            return palin(s.substring(1, s.length() - 1));
        }
        return Math.min(palin(s.substring(1,s.length())), Math.min(palin(s.substring(0,s.length()-1)), palin(s.substring(1, s.length()-1)))) + 1;
    }
}