import java.util.*;

public class Problem_321_NewVilla {
    static HashMap<Integer, boolean[]> hashMap;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int r,d,s;
        boolean[][] doors, switches;
        int villa = 0;
        while(in.hasNext()) {
            hashMap = new HashMap< Integer, boolean[]>();
            r = in.nextInt();
            d = in.nextInt();
            s = in.nextInt();

            if(r == 0 && d == 0 && s == 0)
                break;
            System.out.println("Villa #" + ++villa);

            doors = new boolean[r+1][r+1];

            for (int i = 0; i < d; i++) {
                int x = in.nextInt();
                int y = in.nextInt();
                doors[x][y] = true;
                doors[y][x] = true;
            }

            switches = new boolean[r+1][r+1];
            for (int i = 0; i < s; i++) {
                int x = in.nextInt();
                int y = in.nextInt();
                switches[x][y] = true;
            }

            ArrayList<Integer> visited = new ArrayList<>();
            LinkedList<RoomState> next = new LinkedList<>();
            boolean[] startlights = new boolean[r+1];
            startlights[1] = true;
            boolean success = false;
            RoomState curr = new RoomState(startlights, 1,null);
            next.add(curr);
            while(! next.isEmpty()) {
                curr = next.remove(0);

                if (curr.isGoal) {
                    success = true;
                    break;
                }

                for (int i = 1; i <= r; i++) {
                    if (doors[curr.pos][i] && curr.lights[i]) {
                        boolean[] newlights = curr.lights.clone();
                        RoomState newState = new RoomState(newlights, i, curr);
                        if(! visited.contains((newState.hashCode()))) {
                            next.add(newState);
                            visited.add(newState.hashCode());
                        }
                    }
                    if (switches[curr.pos][i] && i != curr.pos ) {
                        boolean[] newlights = curr.lights.clone();
                        newlights[i] = (newlights[i] ? false : true);
                        RoomState newState = new RoomState(newlights, curr.pos ,curr);
                        if(! visited.contains((newState.hashCode()))) {
                            next.add(newState);
                            visited.add(newState.hashCode());
                        }
                    }
                }
            }

            if(success) {
                Stack<String> actions = new Stack<>();
                while(curr.prev != null) {
                    if(curr.pos != curr.prev.pos) {
                        actions.add("- Move to room " + curr.pos + ".");
                    } else {
                        for (int i = 0; i < curr.lights.length ; i++) {
                            if(curr.lights[i] != curr.prev.lights[i]) {
                                actions.push("- Switch " + (curr.lights[i] ?  "on" : "off") +  " light in room " + i +".");
                            }
                        }
                    }
                    curr = curr.prev;
                }
                int size = actions.size();
                System.out.println("The problem can be solved in " + size + " steps:");
                for (int i = 0; i < size; i++) {
                    System.out.println(actions.pop());
                }
            } else {
                System.out.println("The problem cannot be solved.");
            }
            System.out.println();
        }

    }

    static class RoomState {
        boolean[] lights;
        boolean isGoal;
        RoomState prev;
        int pos;

        RoomState(boolean[] lights, int pos, RoomState prev) {
            this.lights = lights;
            this.pos = pos;
            this.prev = prev;
            boolean[] goal = new boolean[lights.length];
            goal[goal.length-1] = true;
            if(Arrays.equals(lights, goal) && pos == goal.length-1) {
                isGoal = true;
            }
        }

        @Override
        public int hashCode() {
            int hash = 0;
            for (int i = 1; i < lights.length; i++) {
                hash += ( lights[i] ?  1 : 0) << i;
            }
            hash |= pos << 11;

            return hash;
        }
    }

}
