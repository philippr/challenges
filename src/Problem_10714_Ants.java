import java.util.Scanner;

public class Problem_10714_Ants {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int cases = in.nextInt();
        int[] pols = new int[cases];
        int[][] result = new int[cases][2];
        int n_Ants;
        int[] ants;

        for (int i = 0; i < cases; i++) {
            pols[i] = in.nextInt();
            n_Ants = in.nextInt();
            int middle = 0;
            int middle_dist = Integer.MAX_VALUE;
            int furthest_dist = 0;
            int furthest = 0;
            ants = new int[n_Ants];

            for (int j = 0; j < n_Ants; j++) {
                ants[j] = in.nextInt();
                int dist = Math.abs((pols[i] / 2) - ants[j]);

                if(dist > furthest_dist) {
                    furthest_dist = dist;
                    furthest = ants[j];
                }
                if(dist < middle_dist) {
                    middle_dist = dist;
                    middle = ants[j];
                }
            }
            result[i][1] = furthest;
            result[i][0] = middle;
        }

        for (int i = 0; i < result.length; i++) {
            int earliest, latest;
            int middle= Math.abs(pols[i]/2);

            if((middle  > result[i][0]))
                earliest = result[i][0];
            else
                earliest = pols[i]-result[i][0];

            if ((middle > result[i][1]))
                latest = pols[i] - result[i][1];
            else
                latest = result[i][1];

            System.out.println(earliest + " " + latest);
        }
    }
}
