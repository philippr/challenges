import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;

public class Problem_10941_Wordsadjustment {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(in.readLine());
        for (int i = 0; i < cases; i++) {
            String x = in.readLine();
            String y = in.readLine();
            int nSuff = Integer.parseInt(in.readLine());
            String[] Suffis = new String[nSuff];

            for (int j = 0; j < nSuff; j++) {
                Suffis[j] = in.readLine();
            }

            HashSet<String> visited = new HashSet<>();
            LinkedList<String> next = new LinkedList<>();

            if(x.length() > y.length()) {
                next.add(x.substring(y.length()));
            }
            else if(y.length() > x.length()) {
                next.add(y.substring(x.length()));
            }
            else if(x.equals(y)){
                System.out.println(0);
                continue;
            } else {
                System.out.println(-1);
                continue;
            }

            System.out.println(bfs(visited, next, Suffis));

        }
    }

    static int bfs(HashSet<String> visited, LinkedList<String> next, String[] Suffis) {
        int count = 1;
        while(! next.isEmpty()) {
            int lengthtNext = next.size();
            for (int i = 0; i < lengthtNext; i++) {
                String curr = next.poll();

                for (String suff : Suffis) {
                    if (curr.length() == suff.length() && curr.equals(suff)) {
                        return count;
                    } else if (curr.length() < suff.length()) {
                        if (curr.equals(suff.substring(0, curr.length()))) {
                            String newSuff = suff.substring(curr.length());
                            if (!visited.contains(newSuff)) {
                                next.add(newSuff);
                                visited.add(newSuff);
                            }
                        }
                    } else if (curr.length() > suff.length()) {
                        if (curr.substring(0, suff.length()).equals(suff)) {
                            String newSuff = curr.substring(suff.length());
                            if (!visited.contains(newSuff)) {
                                next.add(newSuff);
                                visited.add(newSuff);
                            }
                        }
                    }
                }
            }
            count++;
        }
        return -1;
    }
}