import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

// https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=21&page=show_problem&problem=1918
public class Problem_10977_enchantedForest {


    public static void main(String args[]) throws IOException {

        StringTokenizer tokenizer;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
//        Scanner in = new Scanner(System.in);

        for (String line; (line = in.readLine()) != null; ) {
            tokenizer = new StringTokenizer(line);
            int r = Integer.parseInt(tokenizer.nextToken());
            int c = Integer.parseInt(tokenizer.nextToken());
            if (r == 0 && c == 0)
                return;
            tokenizer = new StringTokenizer(in.readLine());
            int m = Integer.parseInt(tokenizer.nextToken()); //n bloked positions
            node[][] field = new node[r][c];

            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    field[i][j] = new node(i, j, true);
                }
            }

            for (int i = 0; i < m; i++) {
                tokenizer = new StringTokenizer(in.readLine());
                int x = Integer.parseInt(tokenizer.nextToken()) - 1;
                int y = Integer.parseInt(tokenizer.nextToken()) - 1;
                field[x][y].safe = false;
            }

            tokenizer = new StringTokenizer(in.readLine());
            int n = Integer.parseInt(tokenizer.nextToken()); //n Jigglypuffs
            for (int i = 0; i < n; i++) {
                tokenizer = new StringTokenizer(in.readLine());

                int x = Integer.parseInt(tokenizer.nextToken()) - 1;
                int y = Integer.parseInt(tokenizer.nextToken()) - 1;
                int l = Integer.parseInt(tokenizer.nextToken());
                field[x][y].safe = false;

                for (int j = 0; j < r; j++) {
                    for (int k = 0; k < c; k++) {
                        if ((Math.abs(x - j) + Math.abs(y - k)) <= l) {
                            field[j][k].safe = false;
                        }
                    }
                }
            }
//
//            System.out.println();
//            for (node[] lines : field) {
//                for (node i : lines)
//                    System.out.print((i.safe == true ? "+" : "-") + "\t");
//                System.out.println();
//            }


            ArrayList<node> visited = new ArrayList<>();
//            TreeSet<node> next = new TreeSet<>(
//                    (node, t1) -> ((Math.abs(node.x - r) + Math.abs(node.y - c) + node.dist) - ((Math.abs(t1.x - r) - Math.abs(t1.y - c)) + t1.dist) )
//
//            );


            PriorityQueue<node> next = new PriorityQueue<>(
                    new Comparator<node>() {
                        @Override
                        public int compare(node node, node t1) {
                            return ((Math.abs(node.x - r) + Math.abs(node.y - c) + node.dist) - ((Math.abs(t1.x - r) - Math.abs(t1.y - c)) + t1.dist) );
                        }
                    }
            );
//            ArrayList<node> next = new ArrayList<>();
            next.add(field[0][0]);
            boolean possible = false;
            while (!next.isEmpty()) {
                node curr = next.poll();

                int x = curr.x;
                int y = curr.y;
                if (x == r - 1 && y == c - 1) {
                    System.out.println(curr.dist);
                    possible = true;
                    break;
                }

                if(curr.safe && !visited.contains(curr)) {
                    for (int i = -1; i <= 1; i += 2) {
                        if (((x + i) >= 0) && ((x + i) < r) && field[x + i][y].safe && !visited.contains(field[x + i][y])) {
                            field[x + i][y].dist = curr.dist + 1;
                            next.add(field[x + i][y]);
                        }
                    }
                    for (int j = -1; j <= 1; j += 2) {
                        if (((y + j) >= 0) && ((y + j) < c) && field[x][y + j].safe && !visited.contains(field[x][y + j])) {
                            field[x][y + j].dist = curr.dist + 1;
                            next.add(field[x][y + j]);
                        }
                    }
                }
                visited.add(curr);

            }
            if (!possible)
                System.out.println("Impossible.");
        }
    }

    static   class node {
        int x, y, dist;
        boolean safe;

        node(int x, int y, boolean safe) {
            this.x = x;
            this.y = y;
            this.safe = safe;
        }


    }
}
