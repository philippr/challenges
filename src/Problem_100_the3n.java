import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Problem_100_the3n {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        ArrayList<int[]> input = new ArrayList<>();

        while (in.hasNextInt()) {
            int i = in.nextInt();
            int j = in.nextInt();
            input.add(new int[]{i, j});
        }

//        long startTime = System.nanoTime();

        HashMap<Integer, Integer> lookup = new HashMap<>(); //key , length

        for (int i = 0; i < input.size(); i++) {
            int min = input.get(i)[0];
            int max = input.get(i)[1];
            if (min > max) {
                int tmp = max;
                max = min;
                min = tmp;
            }

            int maxCycleLength = 0;
            int cycleLength;

            for (int j = min; j <= max; j++) {
                if (!lookup.containsKey(j)) {
                    long n = j;
                    cycleLength = 1;
                    while (n != 1) {
                        if (n % 2 == 1) {
                            n = 3 * n + 1;
                        } else {
                            n /= 2;
                        }
                        cycleLength++;
                    }
                    lookup.put(j, cycleLength);
                } else {
                    cycleLength = lookup.get(j);
                }
                if (cycleLength > maxCycleLength)
                    maxCycleLength = cycleLength;
            }

            System.out.println(input.get(i)[0] + " " + input.get(i)[1] + " " + maxCycleLength);
        }
        //System.out.println("Runtime: " + ((System.nanoTime() - startTime)/1000000) + "ms");
    }
}
