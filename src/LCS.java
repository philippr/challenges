public class LCS {

    public static void main(String args[]) {

        String[] X = {"A","B","C","B","D","A","B"};
        String[] Y = {"B","D","C","A","B","A"};

        for(int[][] bc : LCSLength(X,Y)) {
            for(int[] line : bc) {
                for(int val : line) {
                    System.out.print(val);
                }
                System.out.println();
            }
            System.out.println(" --- ");
        }
        int[][] b = LCSLength(X,Y)[1];
        printLCS(b, X, X.length  ,Y.length);
    }


    /***
     * returns array with [0]=c and [1]=b
     *
     * b:
     *  0: upleft
     *  1: up
     *  2: left
     *
     */
    public static int[][][] LCSLength(String[] X, String[] Y) {
        int m = X.length;
        int n = Y.length;
        int[][] c = new int[m+1][n+1];
        int[][] b = new int[m+1][n+1];

        for (int i = 0; i < m; i++)
            c[i][0] = 0;
        for (int i = 0; i < n; i++)
            c[0][i] = 0;

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if(X[i-1].equals(Y[j-1])) {
                    c[i][j] = c[i-1][j-1] + 1;
                    b[i][j] = 0;
                } else if (c[i-1][j] >= c[i][j-1]) {
                    c[i][j] = c[i-1][j];
                    b[i][j] = 1;
                } else {
                    c[i][j] = c[i][j-1];
                    b[i][j] = 2;
                }
            }
        }
        return new int[][][]{c,b};
    }

    public static void printLCS(int[][]b, String[] X, int i, int j) {
        if (i == 0 || j == 0)
            return;
        if(b[i][j] == 0) {
            printLCS(b,X,i-1,j-1);
            System.out.print(X[i-1]);
        } else if (b[i][j] == 1) {
            printLCS(b,X,i-1,j);
        } else
            printLCS(b,X,i,j-1);

    }
}
