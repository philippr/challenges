import java.util.Scanner;

public class Problem_10684_TheJackpot { //change classname to "Main" for the judge
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        while((n = in.nextInt()) != 0) {
            int sum = 0;
            int max = 0;
            for (int i = 0; i < n; i++) {
                sum += in.nextInt();
                if(sum < 0)
                    sum = 0;
                if(sum > max)
                    max = sum;
            }
            if(max > 0)
                System.out.println("The maximum winning streak is " + max + ".");
            else
                System.out.println("Losing streak.");
        }
    }
    
}
