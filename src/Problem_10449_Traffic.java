import org.omg.PortableInterceptor.INACTIVE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Problem_10449_Traffic {

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer tokenizer = new StringTokenizer(in.readLine());

        //while(more testcases) {

        int n = Integer.parseInt(tokenizer.nextToken());
        int[] busyness = new int[n];
        for (int i = 0; i < n; i++) {
            Integer.parseInt(tokenizer.nextToken());
        }
        int r = Integer.parseInt((tokenizer=new StringTokenizer(in.readLine())).nextToken());
        int[][] roads = new int[r][2];
        for (int i = 0; i < r; i++) {
            tokenizer=new StringTokenizer(in.readLine());
            roads[i][0]  = Integer.parseInt(tokenizer.nextToken());
            roads[i][1]  = Integer.parseInt((tokenizer.nextToken());
        }

        int nQueries = Integer.parseInt(in.readLine());
        int[] queries = new int[nQueries];
        for (int i = 0; i < nQueries; i++) {
            queries[i] = Integer.parseInt(in.readLine());
        }


    }
}
