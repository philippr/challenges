import java.util.ArrayList;
import java.util.Scanner;


public class Problem_10189_Minesweeper {
    public static void  main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<char[][]> fields = new ArrayList<>();

        while (in.hasNext()) {
            int n = in.nextInt();
            int m = in.nextInt();
            char[][] curr = new char[n][m];
            fields.add(curr);
            for (int i = 0; i < n; i++) {
                String s = in.next();
                curr[i] = s.toCharArray();
            }
        }

        for (int i = 0; i < fields.size(); i++) {
            System.out.println("Field #" + i+1 + ":");
            for (char[] lines : fields.get(i)) {
                for (char c : lines)
                    System.out.print(c);
                System.out.println();
            }
        }

    }

}
